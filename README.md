Buster
======

Super simple, Totally awesome, Brute force **static site generator for**
[Ghost](http://ghost.org).

Start with a clean, no commits Github repository.

*Generate Static Pages. Preview. Deploy to Github Pages.*

Warning! This project is a hack. It's not official. But it works for me.

use it:
==========
[View page](http://www.huar.love/blog/2016/10/30/zai-github-pages-shang-da-jian-ghost-bo-ke-jiao-cheng-yu-xin-de/index.html)

Fix bug
===========
> 1. 2016-12-20 19:02:30   添加命令 "-m -e robots=off -U 'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6' " 

1.`-m -e robots=off` 让wget耍流氓无视robots.txt协议,

2.`-U "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6"`  伪造agent信息

(解決問題：pagination 分頁多時生成靜態頁失敗，也可能其他頁面也會失敗)

Windows fork
============

This fork made using ghost possible on windows. Until original repository owner shows himself and pulls my PR, you can use this version. 
To install, use 
```pip install git+https://github.com/Misiur/buster```


The interface
-------------

```setup [--gh-repo=<repo-url>]```

      Creates a GIT repository inside ``static/`` directory.

```generate [--domain=<local-address>]```

      Generates static pages from locally running Ghost instance.

```preview```

      Preview what's generated on ```localhost:9000```.

```deploy```

      Commits and deploys changes static files to Github repository.

```add-domain <domain-name>```

      Adds CNAME file with custom domain name as required by Github
Pages.

Buster assumes you have ```static/``` folder in your current directory (or
creates one during ```setup``` command). You can specify custom directory
path using ```[--dir=<path>]``` option to any of the above commands.

Don't forget to change your blog URL in config.js in Ghost.


The Installation
----------------

Installing Buster is easy with pip:

    $ pip install buster

You'll then have the wonderful ```buster``` command available.

You could also clone the source and use the ``buster.py`` file directly.

Requirements
------------

-  wget: Use ```brew install wget``` to install wget on your Mac.
   Available by default on most linux distributions.

-  git: Use ```brew install git``` to install git on your Mac.
   ```sudo apt-get install git``` on ubuntu/debian

The following python packages would be installed automatically when
installed via ```pip```:

-  `docopt <https://github.com/docopt/docopt>`__: Creates beautiful
   command line interfaces *easily*.
-  `GitPython <https://github.com/gitpython-developers/GitPython>`__:
   Python interface for GIT.

Ghost. What?
------------

`Ghost <http://ghost.org/features/>`__ is a beautifully designed,
completely customisable and completely `Open
Source <https://github.com/TryGhost/Ghost>`__ **Blogging Platform**. If
you haven't tried it out yet, check it out. You'll love it.

The Ghost Foundation is not-for-profit organization funding open source
software and trying to completely change the world of online publishing.
Consider `donating to Ghost <http://ghost.org/about/donate/>`__.

Buster?
~~~~~~~

Inspired by THE GhostBusters.

.. figure:: http://upload.wikimedia.org/wikipedia/en/c/c7/Ghostbusters_cover.png
   :alt: Ghost Buster Movie Poster

   Ghost Buster Movie

Contributing
------------

Checkout the existing
`issues <https://github.com/axitkhurana/buster/issues>`__ or create a
new one. Pull requests welcome!

--------------

*Made with* `jugaad <http://en.wikipedia.org/wiki/Jugaad>`__ *in*
`Dilli <http://en.wikipedia.org/wiki/Delhi>`__.
